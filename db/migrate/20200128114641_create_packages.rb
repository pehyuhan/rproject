class CreatePackages < ActiveRecord::Migration[5.2]
  def change
    create_table :packages do |t|
      t.string :name
      t.string :version
      t.date :published_date
      t.string :title
      t.text :description
      t.string :authors
      t.string :maintainer_name
      t.string :maintainer_email
    end
  end
end
