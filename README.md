# README

This app runs a recurring task every 12pm to fetch new packages from R-Project and save the neccessary date in Package table.  
*No user interface.

This README would normally document whatever steps are necessary to get the
application up and running.

* Ruby version  
	* ruby-2.3.7

* System dependencies  
	* Rails 5.2.4.1
	* `bundle install`

* Postgresql  
	* `rake db:setup`  
	* `rake db:migrate`  

* Services (job queues, cache servers, search engines, etc.)
	* `whenever --update-crontab --set environment='development'`  
	* Display crontab: `crontab -l`  
	* Test rake task: `rails update:fetch_packages`
	
* Test
	* `rails test test/models/package_test.rb`
