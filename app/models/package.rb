require "open-uri"
require 'rubygems/package'
require "dcf"

class Package < ApplicationRecord
  ALL_PACKAGES_URL = 'https://cran.r-project.org/src/contrib/PACKAGES'

  def fetch_data
    # self.generate_package_urls.each do |package_url|
    self.generate_package_urls[0..50].each do |package_url|
      data = self.process(package_url).first
      package = Package.where(name: data["Package"], version: data["Version"])

      if data.present? && !package.present?
        package = Package.create!(
          name: data["Package"],
          version: data["Version"],
          published_date: data["Date"],
          title: data["Title"],
          description: data["Description"],
          authors: data["Author"],
          maintainer_name: maintainer_name(data["Maintainer"]),
          maintainer_email: maintainer_email(data["Maintainer"])
        )
      end
    end
  end

  def generate_package_urls
    # Parse PACKAGES text file to get package URLs
    urls = []
    response = URI.parse(ALL_PACKAGES_URL).open.read
    packages = response.split("\n\n").collect { |p| YAML.load(p) }
    packages.each do |package|
      package_name = package["Package"]
      package_version = package["Version"]
      urls << "http://cran.r-project.org/src/contrib/#{package_name}_#{package_version}.tar.gz"
    end
    urls
  end

  def process(package_url)
    data = []
    # Occasional 404 error
    begin
      source = open(package_url)
      gz = Zlib::GzipReader.open(source)
      tar_extract = Gem::Package::TarReader.new(gz)
      tar_extract.rewind # The extract has to be rewinded after every iteration
      tar_extract.each do |entry|
        if entry.file?
          if entry.full_name.include? "DESCRIPTION"
            result = entry.read(1024*1024)
            parse = DcfParser.new.parse(result)
            parse.elements.collect do |i|
              paragraph = {}
              i.paragraph.elements.each do |row|
                paragraph[row.field.attribute.text_value] = row.field.value.text_value
              end
              data << paragraph
            end
          end
        end
      end
      tar_extract.close
    rescue OpenURI::HTTPError => error
      response = error.io
      Rails.logger.info "Error: #{response.status}, #{package_url}"
      return
    end
    data
  end

  def maintainer_email(str)
    regex = /(?<=\<).*(?=>)/
    regex.match(str)[0]
  end

  def maintainer_name(str)
    regex = /.+?(?= <)/
    regex.match(str)[0]
  end
end
