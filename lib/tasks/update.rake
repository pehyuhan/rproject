namespace :update do
  desc "fetch new packages data"
  task fetch_packages: :environment do
    Package.new.fetch_data
  end
end
